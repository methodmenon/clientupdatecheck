﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Data;
using ssh = Renci.SshNet;
using MCTlib;

namespace MCTlib
{
    class MCTlib
    {
        /* Just using this as a testing stub */
        public void main(string[] args)
        {
            IntegrationsDB db = new IntegrationsDB();
            db.Connect();
        }
    }
    public class MCTDebug
    {
        public string LogName { get; set; }
        public LogLevel Level { get; set; }
        private StreamWriter LogHandle; // Log Handle
        public enum RunModeEnum { Normal, Dryrun};
        public RunModeEnum RunMode;
        
        public enum OutputMethod
        {
            File,
            Console,
            Both,
            None
        };
        OutputMethod HowToLog { get; set; }
        public enum LogLevel
        {
            None,
            Error,
            Warning,
            Info,
            Debug
        };
        
        ~MCTDebug()
        {
            try
            {
                LogHandle.Close();
            }
            catch
            {
                return; // Just ignore any errors.
            }
            
        }
        public MCTDebug(LogLevel level = LogLevel.Warning, OutputMethod logmethod = OutputMethod.Both, string logname="")
        {
            Level = level;
            HowToLog = logmethod;
            LogName = logname;

            RunMode = RunModeEnum.Normal;

            switch (HowToLog)
            {
                case OutputMethod.Both:
                case OutputMethod.File:
                    if (LogName == "")
                    {
                        Console.WriteLine("Debug: Unable to log to a file without a logname");
                        HowToLog = OutputMethod.Console;
                        LogHandle = null;
                        break;
                    }
                    try
                    {
                        if (!File.Exists(LogName))
                        {
                            LogHandle = new StreamWriter(LogName);
                        }
                        else
                        {
                            LogHandle = new StreamWriter(LogName, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    LogHandle.AutoFlush = true;
                    break;
            }
        }
        public void Warn(string message)
        {
            Log("Warn: " + message, LogLevel.Warning);
            
        }
        public void Error(string message)
        {
            Log("Error: " + message, LogLevel.Error);
            Console.WriteLine("Error: " + message);
        }
        public void Info(string message)
        {
            Log(message, LogLevel.Info);
        }
        public void Debug(string message)
        {
            Log(message, LogLevel.Debug);
        }
        public void Log(string message, LogLevel level = LogLevel.Info)
        {
            if (level > Level)
            {
                return;
            }
            if (HowToLog == OutputMethod.Both || HowToLog == OutputMethod.File)
            {
                LogHandle.WriteLine(message);
            }
            if (HowToLog == OutputMethod.Console || HowToLog == OutputMethod.Both)
            {
                Console.WriteLine(message);
            }
        }
    }

    public class Client
    {
        public string User { get; set; }
        public string Password { get; set; }
        public string Server { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int Id { get; set; }
        public string PipelineName;
        public string DownloadUser { get; set; }
        public string DownloadPassword { get; set; }
        public string DownloadServer { get; set; }
        public string UpdateOption { get; set; }
        public MCTDebug Debug { get; set; }
        public List<string> ClientEmail = new List<string>();
        public List<string> TraderEmail = new List<string>();
        public List<Tuple<string, int>> AllEmail = new List<Tuple<string, int>>();
        private IntegrationsDB db; // Let's keep our database connection around to reuse.
        private BindArgs b; // Let's store our bind variable for clients.id query here. Tired to redoing it!

        public Client(string name, MCTDebug dbg = null)
        {
            BindArgs btmp = new BindArgs();
            btmp.add("@inname", name);
            try
            {
                _getData("GetClientByShortname", btmp);
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Unable to find {0} in clients database", name));
            }
        }
        public Client(int id, MCTDebug dbg = null)
        {
            BindArgs b = new BindArgs();
            b.add("@in_id", id);
            try
            {
                _getData("GetClientById", b);
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Unable to find id {0} in clients database", id));
            }
        }
        private void _getData(string procedure, BindArgs bt)
        {
            db = new IntegrationsDB();

            try
            {
                db.Connect();
            }
            catch
            {
                throw;
            }
            db.Query(procedure, "clients", bt, true);
            if (db.Rows == 0)
            {
                throw new Exception("Unable to find client in database");
            }
            DataRow dr = db.DT.Rows[0];
            User = dr["user"].ToString();
            Password = dr["password"].ToString();
            Server = dr["server"].ToString();
            Id = Convert.ToInt32(dr["id"]);
            Name = dr["name"].ToString();
            PipelineName = dr["pipeline"].ToString();
            DownloadUser = dr["dluser"].ToString();
            ShortName = dr["shortname"].ToString();
            b = new BindArgs();
            b.add("@in_id", Id);

            try
            {
                GetEmail();
            }
            catch (Exception e)
            {
                throw new Exception("Unable to get mail address of client.\n" + e.Message);
            }

        }

        public void GetEmail()
        {
            // Now get related email addresses
            try
            {
                _doSPQuery("GetClientEmail");
            }
            catch
            {
                throw;
            }

            if (db.DT.Rows.Count == 0)
            {
                AllEmail = null;
                return;
            }


            foreach (DataRow dr in db.DT.Rows)
            {
                int type = Convert.ToInt16(dr["type"]);
                string addr = dr["email"].ToString();
                AllEmail.Add(new Tuple<string,int>(addr, type));
                if (type == 0) // Trader = 0
                {
                    TraderEmail.Add(addr);
                }
                else if (type == 1)
                {
                    ClientEmail.Add(addr);
                }
            }
        }
        public List<string> Traders()
        {
            List<string> people = new List<string>();

            if (AllEmail == null)
                return null;

            foreach (var tup in AllEmail)
            {
                if (tup.Item2 == 0) // 0 = Trader
                {
                    people.Add(tup.Item1);
                }
            }
            return people;
        }
        public List<string> Clients()
        {
            List<string> people = new List<string>();

            if (AllEmail == null)
                return null;

            foreach (var tup in AllEmail)
            {
                if (tup.Item2 == 1) // 1 = client
                {
                    people.Add(tup.Item1);
                }
            }
            return people;
        }
        public List<string> Admins()
        {
            List<string> people = new List<string>();

            if (AllEmail == null)
                return null;

            foreach (var tup in AllEmail)
            {
                if (tup.Item2 == 3) // 3 = Admin
                {
                    people.Add(tup.Item1);
                }
            }
            return people;
        }
        // This uses our default bind arguments (@in_id which is the client.id) to do some sort of query 
        // Results of the query will be accessible in db.DT -- Data table. 

        private void _doSPQuery(string proc)
        {
            try
            {
                db.Query(proc, "client", b, true);
            }
            catch { throw; }
        }
        public bool IsExportLocked()
        {
            DateTime ts;
            if (db.RH != null)
                db.RH.Close();

            try
            {
                ts = db.GetDateTime("GetExportLock", b, true);
            }
            catch { throw; }
            if (ts == DateTime.MinValue)
                return false;
            return true;
        }
        public bool ExportLock()
        {
            if (IsExportLocked())
            {
                return false;
            }
            try
            {
                db.NonQuery("LockClientExport", b, true);
            }
            catch { throw; }
          
            return true;

        }
        public void ExportUnlock()
        {
            db.Connect(); // Let's just reconnect, just in case.
            try
            {
                db.NonQuery("UnlockClientExport", b, true);
            }
            catch {
                throw; 
            }
        }
        public bool IsClientLocked()
        {
            int answer;
            try
            {
                answer = db.GetInteger("GetLock", b, true);
            }
            catch { throw; }

            if (answer == 1)
                return true;
            else if (answer == 0)
                return false;
            else
                throw new Exception("Unable to determine lock status for " + ShortName);
        }
        public bool Lock()
        {
            if (!IsClientLocked())
            {
                try
                {
                    db.NonQuery("LockClient", b, true);
                }
                catch { throw; }
            }
            else
            {
                return false;
            }
            return true;
        }
        public void Unlock()
        {
            try
            {
                db.NonQuery("UnlockClient", b, true);
            }
            catch { throw; }
        }
    }    
}
