﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;
using System.IO;
using MCTlib;
using NDesk.Options;




public class Program
{
    //int missing_both = 0;
    static bool verbose;
    static string logts = DateTime.Now.ToString("MMddyyyy");
    static string logfile = string.Format(@"C:\encmct\logs\clientupdatecheck_{0}");

    static void Main(string[] args)
    {
        List<string> extra;

        //string sourceDirectory1 = @"//mctsd/MCTfiles/Trading Desk/Client Files/1Client Pipelines (x)/"; //main pipelines folder
        string sourceDirectory1 = @"\\mctsd/MCTfiles\Trading Desk\Client Files\1Client Pipelines (x)\"; //main pipelines folder
        //string sourceDirectory2 = @"//mctsd/MCTfiles/Trading Desk/Client Files/1Client Pipelines (x)/Busy Pipelines/"; //busy pipelines folder
        string sourceDirectory2 = @"\\mctsd\MCTfiles\Trading Desk\Client Files\1Client Pipelines (x)\Busy Pipelines\"; //busy pipelines folder
        string xls = ".xls";
        string csv = ".csv";
        Error err = new Error("ClientUpdateCheck");

        var p = new OptionSet()
        {
            { "verbose", "Get chatty.\n", v => { if (v != null) verbose = true; } },
        };

        //try
        //{
        //    extra = p.Parse(args);
        //    if (extra.Count > 0)
        //    {
                
        //        err.log("Uknown option in ClientUpdateCheck" + string.Join<string>(", ", extra));
        //        return;
        //    }
        //}
        //catch (Exception ex)
        //{
        //    Console.WriteLine(ex.Message);
        //    err.log(ex.Message);
        //    logErr(ex.Message);
        //    return;
        //}

        
        IntegrationsDB db = new IntegrationsDB();
        db.Connect();
        

        string stm = "SELECT id, shortname, pipeline_name FROM clients WHERE inactive=0 AND is_provider != 1";

        
        
        db.Query(stm, "clients");

        if (db.Rows > 0)
        {
            foreach (DataRow row in db.DSA.Tables["clients"].Rows)
            {
                string pipeline_name = row["pipeline_name"].ToString();
                string shortname = row["shortname"].ToString();
                int client_id = Convert.ToInt32(row["id"].ToString());
                
                BindArgs args_param = new BindArgs();
                

                string xls_extension = pipeline_name + xls;
                string csv_extension = pipeline_name + csv;
                string full_path_xls_1 = sourceDirectory1 + xls_extension; //main pipelines folder
                string full_path_xls_2 = sourceDirectory2 + xls_extension; //busy folder
                string full_path_csv_1 = sourceDirectory1 + csv_extension;
                string full_path_csv_2 = sourceDirectory2 + csv_extension;
                string csv_str_mod_time = null;
                string xls_str_mod_time = null;
                int missing_both = 0;

                csv_str_mod_time = lastUpdateTime(full_path_csv_1, full_path_csv_2);
                if (csv_str_mod_time == null)
                    missing_both += 1;
                xls_str_mod_time = lastUpdateTime(full_path_xls_1, full_path_xls_2);
                if (xls_str_mod_time == null)
                    missing_both += 1;

                if (missing_both == 2)
                {
                    string error_message = "Client " + shortname + " does not have a csv or xls file. Please inspect.";
                    Console.WriteLine("Client {0} does not have a csv or xls file. Please investigate.", shortname);
                    err.log(error_message);
                }

                string updateSQL = @"REPLACE INTO pipelineupdates (client_id, csv_modified, xls_modified) VALUES(@client_id, @csv_mod, @xls_mod) ";
                args_param.add("@client_id", client_id);
                args_param.add("@csv_mod", csv_str_mod_time);
                args_param.add("@xls_mod", xls_str_mod_time);
                db.NonQuery(updateSQL, args_param);


            }
        }

#if (DEBUG)
        Console.WriteLine("Press any key...");
        Console.ReadKey();
#endif

    }

    //function will return latest modified time of a file path or (if applicable) the lastest modified time between 2 file paths
    static string lastUpdateTime(string path1, string path2)
    {
        string str_path_mod_time;
        if (File.Exists(path1))
        {
            DateTime path1_mod_time = File.GetLastWriteTime(path1);
            str_path_mod_time = path1_mod_time.ToString("yyyy-MM-dd HH:mm:ss");
            if (File.Exists(path2))
            {
                DateTime path2_mod_time = File.GetLastWriteTime(path2);
                if (path2_mod_time > path1_mod_time)
                {
                    str_path_mod_time = path2_mod_time.ToString("yyyy-MM-dd HH:mm:ss");
#if (DEBUG)
                    Console.WriteLine("The file {0} is in the busy folder.", path2);
#endif
                }
            }

        }
        else
        {
            str_path_mod_time = null;
        }
        return str_path_mod_time;
    }

    static void logErr(string message)
    {
        string timeStamp = DateTime.Now.ToString("f");
        StreamWriter logh = new StreamWriter(logfile, true);
        logh.WriteLine("{0}: {1}", timeStamp, message);
        if (verbose)
        {
            Console.WriteLine(message);
        }
        logh.Close();
    }
}
